# Yard::Sequel

This is a plugin for Yard to make documenting codebases using Sequel models easier. Sequel has some constructs
that Yard does not recognize, or is at least suboptimal for. One such example is [`dataset_module`](http://sequel.jeremyevans.net/rdoc/classes/Sequel/Model/ClassMethods.html#method-i-dataset_module).
On its own, Yard will completely ignore methods declared in its block. Using an inner module first, passing it
to `dataset_module`, and instructing Yard with `@!parse include Module`  helps a bit, but then the methods are wrongly scoped to the instance, not class. Also ignored are associations, a rather important detail of documented models.

From the need to document these, and based on [`yard-activerecord`](https://github.com/theodorton/yard-activerecord), this was born.

## Installation

Add this line to your application's Gemfile, preferably in a `doc` or `development` group:

```ruby
gem 'yard-sequel'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install yard-sequel

## Usage

When running yardoc, either use the command-line option to load plugins:

```
$ yardoc --plugin sequel
```

Or add it to `.yardopts`

```
--plugin sequel
```

See [YARD's manual](https://github.com/lsegal/yard/blob/master/docs/GettingStarted.md#plugins) for details.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/k3rni/yard-sequel.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
