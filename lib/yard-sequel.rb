require "yard-sequel/version"
require 'yard' unless defined?(YARD)
require 'active_support/inflector'
require 'byebug'

module YARD
  module Sequel
    class DatasetModuleHandler < YARD::Handlers::Ruby::Base
      handles method_call(:dataset_module)
      namespace_only

      def process
        extra_state.group = "Dataset methods"
        parse_block(statement.last.last,
                    namespace: namespace, scope: :class)
      end
    end

    class BaseAssociation < YARD::Handlers::Ruby::Base
      namespace_only

      def process
        namespace.groups << group_name unless namespace.groups.include? group_name

        object = register YARD::CodeObjects::MethodObject.new(namespace, method_name)
        object.group = group_name
        object.docstring = return_description if object.docstring.empty?
        object.docstring.add_tag get_tag(:return, '', class_name)
        object.dynamic = true
      end

      private

      def method_name
        call_params[0]
      end

      def humanize(str)
        ActiveSupport::Inflector.humanize(str)
      end

      def class_name(singularize = false)
        param_size = statement.parameters.size
        if param_size > 2
          i           = 1
          return_this = false
          while i < param_size - 1
            # May want to evaluate doing it this way
            statement.parameters[i].jump(:hash).source =~ /(:class_name\s*=>|class_name:)\s*["']([^"']+)["']/
            return $2 if $2
            i += 1
          end
        end
        if singularize == true
          method_name.camelize.singularize
        else
          method_name.camelize
        end
      end

      def get_tag(tag, text, return_classes = [], name=nil)
        YARD::Tags::Tag.new(tag, text, [return_classes].flatten, name)
      end
    end

    class Singular < BaseAssociation
      def class_name
        super(false)
      end

      def return_description
        "An associated #{method_name.humanize}"
      end
    end

    class Plural < BaseAssociation
      def class_name
        "Sequel::Dataset<#{super(true)}>"
      end

      def return_description
        "A dataset of associated #{method_name.humanize}"
      end
    end

    # TODO: handle these as `associate type, name`

    class OneToMany < Plural
      handles method_call(:one_to_many)

      def group_name
        'One to many relations'
      end
    end

    class ManyToMany < Plural
      handles method_call(:many_to_many)

      def group_name
        'Many to many relations'
      end
    end

    class ManyToOne < Singular
      handles method_call(:many_to_one)

      def group_name
        'Many to one relations'
      end
    end

    class OneToOne < Singular
      handles method_call(:one_to_one)

      def group_name
        'One to One relations'
      end
    end

    class OneThroughOne < Singular
      handles method_call(:one_through_one)

      def group_name
        'One through One relations' # Likely incomplete
      end
    end

    class ColumnAlias < YARD::Handlers::Ruby::Base
      handles method_call(:def_column_alias)

      def process
        aliased, original = call_params[0..2]
        object = register YARD::CodeObjects::MethodObject.new(namespace, aliased)
        object.group = 'Aliased columns'
        object.docstring = "Alias for column <tt>#{original}</tt>"
      end
    end
  end
end
